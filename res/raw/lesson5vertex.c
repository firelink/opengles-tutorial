uniform mat4 u_MVPMatrix;

attribute vec4 a_Position;
attribute vec4 a_Color;
//attribute vec2 a_TexCoord1;

//varying vec2 v_TexCoord1;
varying vec4 v_Color;
		
void main()
{
	v_Color = a_Color;
//	v_TexCoord1 = a_TexCoord1;
	gl_Position = u_MVPMatrix * a_Position;
}
