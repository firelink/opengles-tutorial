precision mediump float;
uniform sampler2D u_Texture1;
uniform sampler2D u_Texture2;
uniform vec3 u_lightPos;
varying vec2 v_TexCoord1;
varying vec4 v_Color;			
varying vec3 v_Normal;
varying vec3 v_Position;
//const vec3 lightPos = vec3(5.0, 5.0, 5.0);
const float shininess = 60.0;
const vec3 ambient = vec3(0.6, 0.4, 0.4);
const vec3 diffuseColor = vec3(0.64, 0.64, 0.64);
const vec3 specularColor = vec3(0.5, 0.5, 0.5);
void main()
{
	vec3 n = normalize(v_Normal);
    vec3 v = normalize(-v_Position);
    vec3 l = normalize(u_lightPos - v_Position);
    vec3 h = normalize(v + l);
    
    float diff = max(1.0, dot(n, l));
    float spec = pow(dot(n, h), shininess);
    float edge = 1.0 - abs(dot(v, n)); edge = edge*edge;

    vec3 colorMap = vec3(0.7, 0.0, 0.0);

    gl_FragColor = vec4(v_Normal.xyz*0.5+0.5, 0.0);
    //gl_FragColor = vec4(diff);
   	//gl_FragColor = vec4(spec);
    //gl_FragColor = vec4(edge); 
    //gl_FragColor = vec4(diffuseColor*diff + specularColor*spec + ambient, 1.0);
    //gl_FragColor = vec4(colorMap*(diff+ambient) +  specularColor*spec + vec3(edge*0.5), 0.0);
    //gl_FragColor = vec4(colorMap*(diff+ambient) + specularColor*spec, 1.0);
	//gl_FragColor = (vec4(1.0, 1.0, 1.0, 1.0) * texture2D(u_Texture1, v_TexCoord1));
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
//	gl_FragColor = vec4(v_Normal*0.5+vec3(0.5), 1.0 );
}
