uniform mat4 u_MVPMatrix;
uniform mat4 u_MVMatrix;

attribute vec4 a_Position;
attribute vec4 a_Color;
attribute vec3 a_Normal;
attribute vec2 a_TexCoord1;
attribute vec2 a_TexCoord2;
varying vec2 v_TexCoord1;
varying vec2 v_TexCoord2;
varying vec4 v_Color;
varying vec3 v_Position;
varying vec3 v_Normal;
		
void main()
{
	v_Position = vec3(u_MVMatrix * a_Position);
	v_Normal = vec3(u_MVMatrix * vec4(a_Normal, 0.0));
	v_Color = a_Color;
	v_TexCoord1 = a_TexCoord1;
	v_TexCoord2 = a_TexCoord2;
	gl_Position = u_MVPMatrix * a_Position;
}
