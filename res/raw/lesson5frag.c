precision mediump float;
uniform sampler2D u_Texture1;
uniform sampler2D u_Texture2;
//varying vec2 v_TexCoord1;
varying vec4 v_Color;			
void main()
{
	gl_FragColor = v_Color;
}
