precision mediump float;
uniform vec3 u_LightPos1;
uniform vec3 u_LightPos2;
uniform sampler2D u_Texture2;
varying vec2 v_TexCoord2;
varying vec4 v_Color;
			
varying vec3 v_Normal;
varying vec3 v_Position;
void main()
{
	float distance1 = length(u_LightPos1 - v_Position);
	float distance2 = length(u_LightPos2 - v_Position);
	
	vec3 lightVector1 = normalize(u_LightPos1 - v_Position);
	vec3 lightVector2 = normalize(u_LightPos2 - v_Position);
	
	float diffuse1 = max(dot(v_Normal, lightVector1), 0.1);
	float diffuse2 = max(dot(v_Normal, lightVector2), 0.1);
	
	diffuse1 = diffuse1 * (1.0 / (1.0 + (0.10 * distance1)));
	diffuse2 = diffuse2 * (1.0 / (1.0 + (0.10 * distance2)));
	
	gl_FragColor = ((diffuse2+diffuse1) * (texture2D(u_Texture2, v_TexCoord2)));
}
