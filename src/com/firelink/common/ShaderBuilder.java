package com.firelink.common;
import android.opengl.*;

public class ShaderBuilder
{
	public static int setProgram(int vertex, int fragment, String[] params)
	{
		int programHandle = GLES20.glCreateProgram();
		if (programHandle != 0)
		{
			//Bind the vertex shader to the program
			GLES20.glAttachShader(programHandle, vertex);

			//Bind the fragment shader to the program
			GLES20.glAttachShader(programHandle, fragment);

			//Bind attributes
			for (int i = 0; i < params.length; i++)
				GLES20.glBindAttribLocation(programHandle, i, params[i]);


			//link two shaders into program
			GLES20.glLinkProgram(programHandle);

			//get link status
			final int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

			//if failure, delete program
			if (linkStatus[0] == 0)
			{
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}

		if (programHandle == 0)
		{
			throw new RuntimeException("Error creating program");
		}

		GLES20.glGetProgramInfoLog(programHandle);

		return programHandle;
	}

	public static int loadShader(int shaderInt, String shader)
	{
		int shaderHandle = GLES20.glCreateShader(shaderInt);
		String error = "";
		if(shaderInt == GLES20.GL_FRAGMENT_SHADER)
			error = "Error creating fragment shader";
		else if(shaderInt == GLES20.GL_VERTEX_SHADER) 
			error = "Error creating vertex shader";
		else
			error = "whoops";

		if (shaderHandle != 0)
		{
			//Pass in shader source
			GLES20.glShaderSource(shaderHandle, shader);

			//Compile the shader
			GLES20.glCompileShader(shaderHandle);

			//Get compile status
			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

			//if compile failed, delete shader
			if (compileStatus[0] == 0)
			{
				GLES20.glDeleteShader(shaderHandle);
				shaderHandle = 0;
			}
		}

		if (shaderHandle == 0)
		{
			throw new RuntimeException(error + " : " + GLES20.glGetShaderInfoLog(shaderHandle));
		}

		return shaderHandle;
	}
}
