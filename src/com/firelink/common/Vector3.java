package com.firelink.common;

import android.util.FloatMath;

public class Vector3 
{
	// Static temporary vector
	private final static Vector3 tmp = new Vector3();

	public float x;
	public float y;
	public float z;
	public float r;

	public Vector3()
	{}

	public Vector3(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3(float x, float y, float z, float r)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.r = r;
	}

	public Vector3(Vector3 v)
	{
		set(v);
	}

	public static float dot(Vector3 v1, Vector3 v2)
	{
		return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	}

	public void setRadius(float r)
	{
		this.r = r;
	}

	public Vector3 copy()
	{
		return new Vector3(this);
	}

	public float length()
	{
		return FloatMath.sqrt(x * x + y * y + z * z);
	}
	public float length2()
	{
		return (x * x + y * y + z * z);
	}

	public Vector3 set(Vector3 v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return this;
	}
	public Vector3 set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}

	public Vector3 subtract(Vector3 v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return this;
	}
	public Vector3 subtract(float x, float y, float z)
	{
		this.x -= x;
		this.y -= y;
		this.z -= z;
		return this;
	}

	public Vector3 normalize()
	{
		float length = length();

		if(length != 0)
		{
			x /= length;
			y /= length;
			z /= length;
		}

		return this;
	}

	public Vector3 add(float x, float y, float z)
	{
		this.x += x;
		this.y += y;
		this.z += z;
		return this;
	}

	public float dotProduct(Vector3 v)
	{
		return x * v.x + y * v.y + z * v.z;
	}

	public float dot(Vector3 v)
	{
		return x * v.x + y * v.y + z * v.z;
	}

	public Vector3 multiply(float scalar)
	{
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return this;
	}

	public Vector3 multiply(Vector3 scale)
	{
		x *= scale.x;
		y *= scale.y;
		z *= scale.z;
		return this;
	}

	public float distance(float x, float y, float z)
	{
		float x_d = x - this.x;
		float y_d = y - this.y;
		float z_d = z - this.z;
		return FloatMath.sqrt(x_d * x_d + y_d * y_d + z_d * z_d);
	}
	public float distance(Vector3 v)
	{
		float x_d = v.x - this.x;
		float y_d = v.y - this.y;
		float z_d = v.z - this.z;
		return FloatMath.sqrt(x_d * x_d + y_d * y_d + z_d * z_d);
	}
	public float distance2(Vector3 v)
	{
		float x_d = v.x - this.x;
		float y_d = v.y - this.y;
		float z_d = v.z - this.z;
		return (x_d * x_d + y_d * y_d + z_d * z_d);
	}

	public String toString()
	{
		return "[" + x + ":" + y + ":" + z + "]";
	}

	public Vector3 tmp()
	{
		return tmp.set(this);
	}

}
