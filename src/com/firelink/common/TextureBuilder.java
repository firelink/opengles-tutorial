package com.firelink.common;
import android.content.*;
import android.graphics.*;
import android.opengl.*;

public class TextureBuilder
{
	public static int loadTexture(final Context context, final int resourceID)
	{
		final int[] textureHandle = new int[1];

		GLES20.glGenTextures(1, textureHandle, 0);

		if(textureHandle[0] != 0)
		{
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inScaled = false; //no pre scaling

			//Read in source
			final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceID, options);

			//Bind to texture
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

			//Set filtering 
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

			//load bitmap into texture
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

			//recycle bitmap, as it has been loaded into OGL
			bitmap.recycle();
		}

		if(textureHandle[0] == 0)
		{
			throw new RuntimeException("Failed to load texture");
		}

		return textureHandle[0];
	}
}
