package com.firelink.opengl;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.*;
import javax.microedition.khronos.egl.*;
import android.content.*;
import java.nio.*;
import android.opengl.GLES20;
import android.graphics.*;
import android.opengl.Matrix;
import android.os.*;
import android.opengl.GLUtils;
import com.firelink.common.*;
import android.content.res.*;
import org.xmlpull.v1.*;
import java.io.*;
import android.widget.*;

//TODO: Changes in loading shader programs


public class ModelLoader1 implements GLSurfaceView.Renderer
{
	private final FloatBuffer mCube1Vertices;
//	private final FloatBuffer mCube1Colors;
	private final FloatBuffer mCube1Normals;
	private final FloatBuffer mCube1TextureCoords1;
	private final ShortBuffer mCube1Indeces;

	private float[] mViewMatrix = new float[16];

	//Used to pass in transformation matrix
	private int mMVPMatrixHandle;
	//pass in model position info
	private int mPositionHandle;
	//used to pass in color info
	private int mColorHandle;
	//used to pass in normal
	private int mNormalHandle;
	/** This will be used to pass in the light position. */
	private int mLightPos1Handle;
	private int mLightPos2Handle;
	//Model view matrix
	private int mMVMatrixHandle;
	//Used to pass in texture \
	private int mTextureUniformHandle1;
	//Used to pass in model texture coordinate info
	private int mTextureCoordHandle1;
	//This is a handle to our texture data
	private int[] mTextureDataHandle = new int[6];

	//Store the projection. this is used to project the scene onto a 2D viewport
	private float[] mProjectionMatrix = new float[16];

	//store the model matrix. this matrix is used to move models from object space (where each model can be thought
	//of being located at the center of the universe) to world space
	private float[] mModelMatrix = new float[16];

	private final int mBytesPerFloat = 4;

	//allocate storage for the final combined matrix. Passed onto the shader program
	private float[] mMVPMatrix = new float[16];
	//size of the position data in elements
	private final int mPositionDataSize = 3;
	//size of color data in elements
	private final int mColorDataSize = 4;
	//size of normal data
	private final int mNormalDataSize = 3;
	//Size of texture coordinate data
	private final int mTextureCoordDataSize = 2;



	private float[] mLightModelMatrix1 = new float[16];
	private float[] mLightModelMatrix2 = new float[16];


	/** Used to hold a light centered on the origin in model space. We need a 4th coordinate so we can get translations to work when
	 *  we multiply this by our transformation matrices. */
	private final float[] mLightPosInModelSpace1 = new float[] {0.0f, 0.0f, 0.0f, 1.0f};
	private final float[] mLightPosInModelSpace2 = new float[] {0.0f, 0.0f, 0.0f, 1.0f};

	/** Used to hold the current position of the light in world space (after transformation via model matrix). */
	private final float[] mLightPosInWorldSpace1 = new float[4];
	private final float[] mLightPosInWorldSpace2 = new float[4];
	/** Used to hold the transformed position of the light in eye space (after transformation via modelview matrix) */
	private final float[] mLightPosInEyeSpace1 = new float[4];
	private final float[] mLightPosInEyeSpace2 = new float[4];
	/** This is a handle to our per-vertex cube shading program. */
	private int mPerVertexProgramHandle;

	/** This is a handle to our light point program. */
	private int mPointProgramHandle;

	private Context mActivityContext;

	private XmlResourceParser xrp;

	private int vertexCount;
	private int faceCount;
	private float xRotation = 0;
	private double screenWidth;
	public volatile float mDeltaX;
	public volatile float mDeltaY;

	private float[] mTemporaryMatrix = new float[16];

	private float[] mCurrentRotation = new float[16];

	private float[] mAccumulatedRotation = new float[16];

	private boolean lightSwitch;

	public ModelLoader1(Context c)
	{
		mActivityContext = c;

		xrp = c.getResources().getXml(R.xml.model1);

		float[] cube1PositionData = null;
		float[] cube1ColorData = null;
		float[] cube1NormalData = null;
		float[] cube1TextureData1 = null;
		short[] cube1Indices = null;
		int positionIndex = 0;
		int colorIndex = 0;
		int normalIndex = 0;
		int textureIndex = 0;
		int faceIndex = 0;

		try
		{
			while (xrp.getEventType() != XmlResourceParser.END_DOCUMENT)
			{
				if (xrp.getEventType() == XmlResourceParser.START_TAG)
				{
					String s = xrp.getName();
					if (s.equals("faces"))
					{
						int i = xrp.getAttributeIntValue(null, "count", 0);
						//length of trianges array
						faceCount = i * 3;
						cube1Indices = new short[faceCount];
					}
					if (s.equals("sharedgeometry"))
					{
						int i = xrp.getAttributeIntValue(null, "vertexcount", 0);
						//vertex count
						vertexCount = i / 3;
						cube1PositionData = new float[i * 3];
						cube1NormalData = new float[i * 3];
						cube1ColorData = new float[i * 4];
						cube1TextureData1 = new float[i * 2];
					}
					if (s.equals("position"))
					{
						float x = xrp.getAttributeFloatValue(null, "x", 0);
						float y = xrp.getAttributeFloatValue(null, "y", 0);
						float z = xrp.getAttributeFloatValue(null, "z", 0);

						if (cube1PositionData != null)
						{
							cube1PositionData[positionIndex++] = x;
							cube1PositionData[positionIndex++] = y;
							cube1PositionData[positionIndex++] = z;
						}
					}
					if (s.equals("normal"))
					{
						float x = xrp.getAttributeFloatValue(null, "x", 0);
						float y = xrp.getAttributeFloatValue(null, "y", 0);
						float z = xrp.getAttributeFloatValue(null, "z", 0);

						if (cube1NormalData != null)
						{
							cube1NormalData[normalIndex++] = x;
							cube1NormalData[normalIndex++] = y;
							cube1NormalData[normalIndex++] = z;
						}
					}
					if (s.equals("texcoord"))
					{
						float u = xrp.getAttributeFloatValue(null, "u", 0);
						float v = xrp.getAttributeFloatValue(null, "v", 0);
//						float z = xrp.getAttributeFloatValue(null, "z", 0);

						if (cube1TextureData1 != null)
						{
							cube1TextureData1[textureIndex++] = u;
							cube1TextureData1[textureIndex++] = v;
							//cube1TextureData1[textureIndex++] = z;
						}
					}
					if (s.equals("face"))
					{
						short v1 = (short)xrp.getAttributeIntValue(null, "v1", 0);
						short v2 = (short)xrp.getAttributeIntValue(null, "v2", 0);
						short v3 = (short)xrp.getAttributeIntValue(null, "v3", 0);

						if (cube1Indices != null)
						{
							cube1Indices[faceIndex++] = v1;
							cube1Indices[faceIndex++] = v2;
							cube1Indices[faceIndex++] = v3;
						}
					}
				}
				else if (xrp.getEventType() == XmlResourceParser.END_TAG)
				{}
				else if (xrp.getEventType() == XmlResourceParser.TEXT)
				{}
				xrp.next();
			}
			xrp.close();
		}
		catch (XmlPullParserException e)
		{}
		catch (IOException q)
		{}


		//initialize the buffers
		mCube1Vertices = ByteBuffer.allocateDirect(cube1PositionData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
//		mCube1Colors = ByteBuffer.allocateDirect(cube1ColorData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mCube1Normals = ByteBuffer.allocateDirect(cube1NormalData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mCube1TextureCoords1 = ByteBuffer.allocateDirect(cube1TextureData1.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mCube1Indeces = ByteBuffer.allocateDirect(cube1Indices.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asShortBuffer();


		mCube1Indeces.put(cube1Indices).position(0);
		mCube1Vertices.put(cube1PositionData).position(0);
//		mCube1Colors.put(cube1ColorData).position(0);
		mCube1Normals.put(cube1NormalData).position(0);
		mCube1TextureCoords1.put(cube1TextureData1).position(0);
	}

	public void onSurfaceCreated(GL10 p1, EGLConfig p2)
	{
		//Set background color to gray
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		//disable culling and depth testing
		GLES20.glEnable(GLES20.GL_CULL_FACE);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

//		GLES20.glEnable(GLES20.GL_BLEND);
//		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);

		//position the eye behind the origin
		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = -0.5f;

		//looking towards the distance
		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = -5.0f;

		//Set up the vector: This is where our head would be pointing were we holding the camera
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		//Set the view matrix. This can be said to be the camera's position
		//NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model
		//and a view matrix. In OGL2, we can keep track of these matrices separately if we choose
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);


		//Create a program object and store the handle to it
		//int programHandle = GLES20.glCreateProgram();
		mPerVertexProgramHandle = ShaderBuilder.setProgram(ShaderBuilder.loadShader(GLES20.GL_VERTEX_SHADER, getVertexShader()), 
														   ShaderBuilder.loadShader(GLES20.GL_FRAGMENT_SHADER, getPixelShader()), 
														   new String[]{"a_Position", "a_Color", "a_TexCoord1", "a_Normal"});

														   
		// Define a simple shader program for our point.
		final String pointVertexShader =
			"uniform mat4 u_MVPMatrix;      \n"		
			+	"attribute vec4 a_Position;     \n"		
			+ "void main()                    \n"
			+ "{                              \n"
			+ "   gl_Position = u_MVPMatrix   \n"
			+ "               * a_Position;   \n"
			+ "   gl_PointSize = 10.0;         \n"
			+ "}                              \n";

		final String pointFragmentShader = 
			"precision mediump float;       \n"					          
			+ "void main()                    \n"
			+ "{                              \n"
			+ "   gl_FragColor = vec4(1.0,    \n" 
			+ "   1.0, 0.0, 1.0);             \n"
			+ "}                              \n";

		mPointProgramHandle = ShaderBuilder.setProgram(ShaderBuilder.loadShader(GLES20.GL_VERTEX_SHADER, pointVertexShader), 
													   ShaderBuilder.loadShader(GLES20.GL_FRAGMENT_SHADER, pointFragmentShader), 
													   new String[] {"a_Position"}); 
		
		
		mTextureDataHandle[0] = TextureBuilder.loadTexture(mActivityContext, R.drawable.texture2);
		mTextureDataHandle[1] = TextureBuilder.loadTexture(mActivityContext, R.drawable.texture3);
		mTextureDataHandle[2] = TextureBuilder.loadTexture(mActivityContext, R.drawable.commander);
		mTextureDataHandle[3] = TextureBuilder.loadTexture(mActivityContext, R.drawable.texture5);
		mTextureDataHandle[4] = TextureBuilder.loadTexture(mActivityContext, R.drawable.texture6);
		mTextureDataHandle[5] = TextureBuilder.loadTexture(mActivityContext, R.drawable.texture7);
	}

	public void onSurfaceChanged(GL10 p1, int width, int height)
	{
		//Set OGL viewport
		GLES20.glViewport(0, 0, width, height);
		screenWidth = width;

		//Create a new perspective projection matrix. The height will stay the same
		//while the width will vary as per aspect ratio
		final float ratio = (float)width / height;
		final float left = -ratio;
		final float right = ratio;
		final float bottom = -1.0f;
		final float top = 1.0f;
		final float near = 1.0f;
		final float far = 10.0f;

		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);

		Matrix.setIdentityM(mAccumulatedRotation, 0);
	}
	

	public void onDrawFrame(GL10 p1)
	{
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		
		//position the eye behind the origin
		final float eyeX = 0.0f;// + mDeltaX;
		final float eyeY = 0.0f;// + mDeltaY;
		final float eyeZ = -0.5f;

		//looking towards the distance
		final float lookX = 0.0f;// + mDeltaX;
		final float lookY = 0.0f;// + mDeltaY;
		final float lookZ = -5.0f;

		//Set up the vector: This is where our head would be pointing were we holding the camera
		final float upX = 0.0f;// + mDeltaX;
		final float upY = 1.0f;// + mDeltaY;
		final float upZ = 0.0f;

		//Set the view matrix. This can be said to be the camera's position
		//NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model
		//and a view matrix. In OGL2, we can keep track of these matrices separately if we choose
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);

		//complete rotaton every 10 seconds
		long time = SystemClock.uptimeMillis() % 10000L;
		long time2 = SystemClock.uptimeMillis() % 15000L;

		float angle = (360.0f / 10000.0f) * ((int)time);
		float angle2 = (360.0f / 15000.0f) * ((int)time2);


		//set our per-vertex lighting program
		GLES20.glUseProgram(mPerVertexProgramHandle);

		//set program handles for cube drawing
		mMVPMatrixHandle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_MVPMatrix");
		mMVMatrixHandle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_MVMatrix");
		mLightPos1Handle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_lightPos");
//		mLightPos2Handle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_LightPos2");
		mTextureUniformHandle1 = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_Texture1");
		mTextureCoordHandle1 = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_TexCoord1");
		mPositionHandle = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_Position");
		mColorHandle = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_Color");
		mNormalHandle = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_Normal");

		//Texture stuff
		//Set active texture
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		//Binding
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle[2]);
		//Tell texture uniform sampler to use this texture in the shader by binding to texture unit 0
		GLES20.glUniform1i(mTextureUniformHandle1, 0);
		
			Matrix.setIdentityM(mLightModelMatrix1, 0);
			Matrix.translateM(mLightModelMatrix1, 0, 0.0f, 0.0f, -7.0f);
			Matrix.rotateM(mLightModelMatrix1, 0, angle, 0.0f, 0.5f, 2.0f);
			Matrix.translateM(mLightModelMatrix1, 0, 0.0f, 0.0f, 4.0f);

			Matrix.multiplyMV(mLightPosInWorldSpace1, 0, mLightModelMatrix1, 0, mLightPosInModelSpace1, 0);
			Matrix.multiplyMV(mLightPosInEyeSpace1, 0, mViewMatrix, 0, mLightPosInWorldSpace1, 0);

		//light source 2
		if (false)
		{
			Matrix.setIdentityM(mLightModelMatrix2, 0);
			Matrix.translateM(mLightModelMatrix2, 0, 0.0f, 0.0f, -5.0f);
			Matrix.rotateM(mLightModelMatrix2, 0, angle, 0.0f, 2.0f, 0.0f);
			Matrix.translateM(mLightModelMatrix2, 0, 0.0f, 0.0f, 2.0f);

			Matrix.multiplyMV(mLightPosInWorldSpace2, 0, mLightModelMatrix2, 0, mLightPosInModelSpace2, 0);
			Matrix.multiplyMV(mLightPosInEyeSpace2, 0, mViewMatrix, 0, mLightPosInWorldSpace2, 0);
		}

		//Draw cubes
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0.0f, -2.0f, -4.0f);
		Matrix.rotateM(mModelMatrix, 0, 90, 1.0f, 0.0f, 0.0f);
//		Matrix.scaleM(mModelMatrix, 0, 1.0f, 1.0f, 0.5f);

		Matrix.setIdentityM(mCurrentRotation, 0);
		Matrix.rotateM(mCurrentRotation, 0, mDeltaX, 0.0f, 0.0f, 1.0f);
//		Matrix.rotateM(mCurrentRotation, 0, mDeltaY, 1.0f, 0.0f, 0.0f);
    	mDeltaX = 0.0f;
    	mDeltaY = 0.0f;

		// Multiply the current rotation by the accumulated rotation, and then set the accumulated rotation to the result.
    	Matrix.multiplyMM(mTemporaryMatrix, 0, mCurrentRotation, 0, mAccumulatedRotation, 0);
    	System.arraycopy(mTemporaryMatrix, 0, mAccumulatedRotation, 0, 16);

        // Rotate the cube taking the overall rotation into account.     	
    	Matrix.multiplyMM(mTemporaryMatrix, 0, mModelMatrix, 0, mAccumulatedRotation, 0);
    	System.arraycopy(mTemporaryMatrix, 0, mModelMatrix, 0, 16);

		drawCube();


		GLES20.glUseProgram(mPointProgramHandle);
		drawLight();
//		drawLight2();
	}

	private void drawCube()
	{
		GLES20.glFrontFace(GLES20.GL_CCW);
		//pass in position info
		mCube1Vertices.position(0);
		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, 0, mCube1Vertices);
		GLES20.glEnableVertexAttribArray(mPositionHandle);
		//pass in color info
//		mCube1Colors.position(0);
//		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 0, mCube1Colors);
//		GLES20.glEnableVertexAttribArray(mColorHandle);
		//pass in normal info
		mCube1Normals.position(0);
		GLES20.glVertexAttribPointer(mNormalHandle, mNormalDataSize, GLES20.GL_FLOAT, false, 0, mCube1Normals);
		GLES20.glEnableVertexAttribArray(mNormalHandle);
		//pass in texture info
		mCube1TextureCoords1.position(0);
		GLES20.glVertexAttribPointer(mTextureCoordHandle1, mTextureCoordDataSize, GLES20.GL_FLOAT, false, 0, mCube1TextureCoords1);
		GLES20.glEnableVertexAttribArray(mTextureCoordHandle1);
		//pass in texture info
//		mCube1TextureCoords2.position(0);
//		GLES20.glVertexAttribPointer(mTextureCoordHandle2, mTextureCoordDataSize, GLES20.GL_FLOAT, false, 0, mCube1TextureCoords2);
//		GLES20.glEnableVertexAttribArray(mTextureCoordHandle2);
		//this multiplies the view matrix by the model matrix and stores the result in the MVP matrix
		//which currently contains model * view
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
		//pass in the modelview matrix
		GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);
		//this multiplies the modelview matrix by the projection matrix and stores the result in the MVP matrix
		//which now holds model * view * projection
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		Matrix.multiplyMM(mTemporaryMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
        System.arraycopy(mTemporaryMatrix, 0, mMVPMatrix, 0, 16);

//		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		GLES20.glUniform3f(mLightPos1Handle, mLightPosInEyeSpace1[0], mLightPosInEyeSpace1[1], mLightPosInEyeSpace1[2]);
//		GLES20.glUniform3f(mLightPos2Handle, mLightPosInEyeSpace2[0], mLightPosInEyeSpace2[1], mLightPosInEyeSpace2[2]);

//		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, faceCount);
//		GLES20.glVertexAttribPointer(
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, faceCount, GLES20.GL_UNSIGNED_SHORT, mCube1Indeces);
	}

	/*	private void drawHeart()
	 {

	 mHeart.position(0);
	 GLES20.glVertexAttribPointer(mHeartPosHandle, 3, GLES20.GL_FLOAT, false, 0, mHeart);
	 GLES20.glEnableVertexAttribArray(mHeartPosHandle);

	 mHeartTexCoords.position(0);
	 GLES20.glVertexAttribPointer(mTextureCoordHandle2, 2, GLES20.GL_FLOAT, false, 0, mHeartTexCoords);
	 GLES20.glEnableVertexAttribArray(mTextureCoordHandle2);

	 mHeartNormals.position(0);
	 GLES20.glVertexAttribPointer(mHeartNormalHandle, mNormalDataSize, GLES20.GL_FLOAT, false, 0, mHeartNormals);
	 GLES20.glEnableVertexAttribArray(mHeartNormalHandle);

	 // Pass in the transformation matrix.
	 Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mHeartMatrix, 0);
	 GLES20.glUniformMatrix4fv(heartMVMatrixHandle, 1, false, mMVPMatrix, 0);
	 Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
	 GLES20.glUniformMatrix4fv(heartMVPMatrixHandle, 1, false, mMVPMatrix, 0);

	 GLES20.glUniform3f(mHeartLightPos1Handle, mLightPosInEyeSpace1[0], mLightPosInEyeSpace1[1], mLightPosInEyeSpace1[2]);
	 GLES20.glUniform3f(mHeartLightPos2Handle, mLightPosInEyeSpace2[0], mLightPosInEyeSpace2[1], mLightPosInEyeSpace2[2]);

	 // Draw the point.
	 GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, heartArraySize);
	 }
	 */
	private void drawLight()
	{
		final int pointMVPMatrixHandle = GLES20.glGetUniformLocation(mPointProgramHandle, "u_MVPMatrix");
		final int pointPositionHandle = GLES20.glGetAttribLocation(mPointProgramHandle, "a_Position");

		// Pass in the position.
		GLES20.glVertexAttrib3f(pointPositionHandle, mLightPosInModelSpace1[0], mLightPosInModelSpace1[1], mLightPosInModelSpace1[2]);

		// Since we are not using a buffer object, disable vertex arrays for this attribute.
		GLES20.glDisableVertexAttribArray(pointPositionHandle);  

		// Pass in the transformation matrix.
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mLightModelMatrix1, 0);
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(pointMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		// Draw the point.
		GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
	}

	private void drawLight2()
	{
		final int pointMVPMatrixHandle = GLES20.glGetUniformLocation(mPointProgramHandle, "u_MVPMatrix");
		final int pointPositionHandle = GLES20.glGetAttribLocation(mPointProgramHandle, "a_Position");

		// Pass in the position.
		GLES20.glVertexAttrib3f(pointPositionHandle, mLightPosInModelSpace2[0], mLightPosInModelSpace2[1], mLightPosInModelSpace2[2]);

		// Since we are not using a buffer object, disable vertex arrays for this attribute.
		GLES20.glDisableVertexAttribArray(pointPositionHandle);  

		// Pass in the transformation matrix.
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mLightModelMatrix2, 0);
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(pointMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		// Draw the point.
		GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
	}


	protected String getVertexShader()
	{
		final String vertexShader = RawResourceReader.readTextFileFromRawResource(mActivityContext, R.raw.model1vertex);


		return vertexShader;
	}

	protected String getPixelShader()
	{
		final String  fragmentShader = RawResourceReader.readTextFileFromRawResource(mActivityContext, R.raw.model1frag);

		return fragmentShader;
	}
}
