package com.firelink.opengl;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.opengl.*;
import android.content.*;
import android.content.pm.*;
import android.util.*;

public class LessonActivity extends Activity
{

	private GLSurfaceView mGLSurfaceView;
	//private ModelLoader2GLSurfaceView mGLSurfaceView2;
	private ModelLoader1GLSurfaceView mGLSurfaceView1;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
		
		//mGLSurfaceView = new GLSurfaceView(this);
		mGLSurfaceView1 = new ModelLoader1GLSurfaceView(this);
//		mGLSurfaceView2 = new LessonFiveGLSurfaceView(this);
		
		DisplayMetrics dsp = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dsp);
		//mGLSurfaceView2 = new ModelLoader2GLSurfaceView(this);
		
		//check to see if the system supports OGLES20
		final ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configInfo = activityManager.getDeviceConfigurationInfo();
		final boolean supportsES2 = configInfo.reqGlEsVersion >= 0x20000;
		
		if(supportsES2)
		{
			//Request OGL ES2 compatible context
			//mGLSurfaceView2.setEGLContextClientVersion(2);
			mGLSurfaceView1.setEGLContextClientVersion(2);
			//set renderer
			mGLSurfaceView1.setRenderer(new ModelLoader1(this));
	//		mGLSurfaceView2.setRenderer(new ModelLoader2(this), dsp.density);
			
		}
		else
		{
			//ES 1.x compatible stuff here
		}
		
		setContentView(mGLSurfaceView1);
    }
	
	protected void onResume()
	{
		super.onResume();
		mGLSurfaceView1.onResume();
	}
	
	protected void onPause()
	{
		super.onPause();
		mGLSurfaceView1.onPause();
	}
}
