package com.firelink.opengl;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.*;
import javax.microedition.khronos.egl.*;
import android.content.*;
import java.nio.*;
import android.opengl.GLES20;
import android.graphics.*;
import android.opengl.Matrix;
import android.os.*;


//TODO: Changes in loading shader programs


public class LessonThreeRenderer implements GLSurfaceView.Renderer
{
	private final FloatBuffer mCube1Vertices;
	private final FloatBuffer mCube1Colors;
	private final FloatBuffer mCube1Normals;

	private float[] mViewMatrix = new float[16];

	//Used to pass in transformation matrix
	private int mMVPMatrixHandle;
	//pass in model position info
	private int mPositionHandle;
	//used to pass in color info
	private int mColorHandle;
	//used to pass in normal
	private int mNormalHandle;
	/** This will be used to pass in the light position. */
	private int mLightPos1Handle;
	private int mLightPos2Handle;
	//jjj
	private int mMVMatrixHandle;

	//Store the projection. this is used to project the scene onto a 2D viewport
	private float[] mProjectionMatrix = new float[16];

	//store the model matrix. this matrix is used to move models from object space (where each model can be thought
	//of being located at the center of the universe) to world space
	private float[] mModelMatrix = new float[16];

	private final int mBytesPerFloat = 4;

	//allocate storage for the final combined matrix. Passed onto the shader program
	private float[] mMVPMatrix = new float[16];
	//size of the position data in elements
	private final int mPositionDataSize = 3;
	//size of color data in elements
	private final int mColorDataSize = 4;
	//size of normal data
	private final int mNormalDataSize = 3;



	private float[] mLightModelMatrix1 = new float[16];
	private float[] mLightModelMatrix2 = new float[16];



	/** Used to hold a light centered on the origin in model space. We need a 4th coordinate so we can get translations to work when
	 *  we multiply this by our transformation matrices. */
	private final float[] mLightPosInModelSpace1 = new float[] {0.0f, 0.0f, 0.0f, 1.0f};
	private final float[] mLightPosInModelSpace2 = new float[] {0.0f, 0.0f, 0.0f, 1.0f};

	/** Used to hold the current position of the light in world space (after transformation via model matrix). */
	private final float[] mLightPosInWorldSpace1 = new float[4];
	private final float[] mLightPosInWorldSpace2 = new float[4];
	/** Used to hold the transformed position of the light in eye space (after transformation via modelview matrix) */
	private final float[] mLightPosInEyeSpace1 = new float[4];
	private final float[] mLightPosInEyeSpace2 = new float[4];
	/** This is a handle to our per-vertex cube shading program. */
	private int mPerVertexProgramHandle;

	/** This is a handle to our light point program. */
	private int mPointProgramHandle;

	public LessonThreeRenderer(Context c)
	{
		//This triangle is red, green and blue
		final float[] cube1PositionData = {
			//In OpenGL counter-clockwise winding is default. This means that when we look at a triangle
			//if the points are counter-clockwise we are looking at the front. if not we are looking at
			//the back. OpenGL has an optimization where all back-facing triangles are culled, since they
			//usually represent the backside of an object and aren't visible anyways


			//Front-face
			-1.0f,		1.0f,		1.0f,
			-1.0f,		-1.0f,		1.0f,
			1.0f,		1.0f,		1.0f,
			-1.0f,		-1.0f,		1.0f,
			1.0f,		-1.0f,		1.0f,
			1.0f,		1.0f,		1.0f,

			// Right face
			1.0f,		1.0f,		1.0f,				
			1.0f,		-1.0f,		1.0f,
			1.0f,		1.0f,		-1.0f,
			1.0f,		-1.0f,		1.0f,				
			1.0f,		-1.0f,		-1.0f,
			1.0f,		1.0f,		-1.0f,

			// Back face
			1.0f,		1.0f,		-1.0f,				
			1.0f,		-1.0f,		-1.0f,
			-1.0f,		1.0f,		-1.0f,
			1.0f,		-1.0f,		-1.0f,				
			-1.0f,		-1.0f,		-1.0f,
			-1.0f,		1.0f,		-1.0f,

			// Left face
			-1.0f,		1.0f,		-1.0f,				
			-1.0f,		-1.0f,		-1.0f,
			-1.0f,		1.0f,		1.0f, 
			-1.0f,		-1.0f,		-1.0f,				
			-1.0f,		-1.0f,		1.0f, 
			-1.0f,		1.0f,		1.0f, 

			// Top face
			-1.0f,		1.0f,		-1.0f,				
			-1.0f,		1.0f,		1.0f, 
			1.0f,		1.0f,		-1.0f, 
			-1.0f,		1.0f,		1.0f, 				
			1.0f,		1.0f,		1.0f, 
			1.0f,		1.0f,		-1.0f,

			// Bottom face
			1.0f,		-1.0f,		-1.0f,				
			1.0f,		-1.0f,		1.0f, 
			-1.0f,		-1.0f,		-1.0f,
			1.0f,		-1.0f,		1.0f, 				
			-1.0f,		-1.0f,		1.0f,
			-1.0f,		-1.0f,		-1.0f,
		};

		final float[] cube1ColorData = {
			// Front face (red)
			1.0f, 0.0f, 0.0f, 1.0f,				
			1.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f, 1.0f,				
			1.0f, 0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f, 1.0f,

			// Right face (green)
			0.0f, 1.0f, 0.0f, 1.0f,				
			0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 1.0f,				
			0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 1.0f,

			// Back face (blue)
			0.0f, 0.0f, 1.0f, 1.0f,				
			0.0f, 0.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f,				
			0.0f, 0.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f,

			// Left face (yellow)
			1.0f, 1.0f, 0.0f, 1.0f,				
			1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,				
			1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,

			// Top face (cyan)
			0.0f, 1.0f, 1.0f, 1.0f,				
			0.0f, 1.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f, 1.0f,				
			0.0f, 1.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f, 1.0f,

			// Bottom face (magenta)
			1.0f, 0.0f, 1.0f, 1.0f,				
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 1.0f,				
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 1.0f
		};

		// X, Y, Z
		// The normal is used in light calculations and is a vector which points
		// orthogonal to the plane of the surface. For a cube model, the normals
		// should be orthogonal to the points of each face.
		final float[] cube1NormalData = {
			// Front face
			0.0f, 0.0f, 1.0f,				
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,				
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			// Right face 
			1.0f, 0.0f, 0.0f,				
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,				
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,

			// Back face 
			0.0f, 0.0f, -1.0f,				
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,				
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,

			// Left face 
			-1.0f, 0.0f, 0.0f,				
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,				
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,

			// Top face 
			0.0f, 1.0f, 0.0f,			
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,				
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			// Bottom face 
			0.0f, -1.0f, 0.0f,			
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,				
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f
		};

		//initialize the buffers
		mCube1Vertices = ByteBuffer.allocateDirect(cube1PositionData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mCube1Colors = ByteBuffer.allocateDirect(cube1ColorData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mCube1Normals = ByteBuffer.allocateDirect(cube1NormalData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();

		mCube1Vertices.put(cube1PositionData).position(0);
		mCube1Colors.put(cube1ColorData).position(0);
		mCube1Normals.put(cube1NormalData).position(0);
	}

	public void onSurfaceCreated(GL10 p1, EGLConfig p2)
	{
		//Set background color to gray
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		//use culling to remove back-faces
		GLES20.glEnable(GLES20.GL_CULL_FACE);
		//enable depth testing 
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);


		//position the eye behind the origin
		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = -0.5f;

		//looking towards the distance
		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = -5.0f;

		//Set up the vector: This is where our head would be pointing were we holding the camera
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		//Set the view matrix. This can be said to be the camera's position
		//NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model
		//and a view matrix. In OGL2, we can keep track of these matrices separately if we choose
		android.opengl.Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);


		final String vertexShader = getVertexShader();
		final String fragmentShader = getPixelShader();


		int vertexShaderHandle = loadShader(GLES20.GL_VERTEX_SHADER, vertexShader);

		int fragmentShaderHandle = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);


		//Create a program object and store the handle to it
		//int programHandle = GLES20.glCreateProgram();
		mPerVertexProgramHandle = setProgram(vertexShaderHandle, fragmentShaderHandle, new String[]{"a_Position", "a_Color", "a_Normal"});

		// Define a simple shader program for our point.
		final String pointVertexShader =
			"uniform mat4 u_MVPMatrix;      \n"		
			+	"attribute vec4 a_Position;     \n"		
			+ "void main()                    \n"
			+ "{                              \n"
			+ "   gl_Position = u_MVPMatrix   \n"
			+ "               * a_Position;   \n"
			+ "   gl_PointSize = 5.0;         \n"
			+ "}                              \n";

		final String pointFragmentShader = 
			"precision mediump float;       \n"					          
			+ "void main()                    \n"
			+ "{                              \n"
			+ "   gl_FragColor = vec4(1.0,    \n" 
			+ "   1.0, 1.0, 1.0);             \n"
			+ "}                              \n";

		final int pointVertexShaderHandle = loadShader(GLES20.GL_VERTEX_SHADER, pointVertexShader);
		final int pointFragmentShaderHandle = loadShader(GLES20.GL_FRAGMENT_SHADER, pointFragmentShader);
		mPointProgramHandle = setProgram(pointVertexShaderHandle, pointFragmentShaderHandle, new String[] {"a_Position"}); 
	}

	public void onSurfaceChanged(GL10 p1, int width, int height)
	{
		//Set OGL viewport
		GLES20.glViewport(0, 0, width, height);

		//Create a new perspective projection matrix. The height will stay the same
		//while the width will vary as per aspect ratio
		final float ratio = (float)width / height;
		final float left = -ratio;
		final float right = ratio;
		final float bottom = -1.0f;
		final float top = 1.0f;
		final float near = 1.0f;
		final float far = 10.0f;

		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}

	public void onDrawFrame(GL10 p1)
	{
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

		//complete rotaton every 10 seconds
		long time = SystemClock.uptimeMillis() % 10000L;
		long time2 = SystemClock.uptimeMillis() % 15000L;

		float angle = (360.0f / 10000.0f) * ((int)time);
		float angle2 = (360.0f / 15000.0f) * ((int)time2);


		//set our per-vertex lighting program
		GLES20.glUseProgram(mPerVertexProgramHandle);

		//set program handles for cube drawing
		mMVPMatrixHandle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_MVPMatrix");
		mMVMatrixHandle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_MVMatrix");
		mLightPos1Handle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_LightPos1");
		mLightPos2Handle = GLES20.glGetUniformLocation(mPerVertexProgramHandle, "u_LightPos2");
		mPositionHandle = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_Position");
		mColorHandle = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_Color");
		mNormalHandle = GLES20.glGetAttribLocation(mPerVertexProgramHandle, "a_Normal");

		//Calculate position of light, rotate and then push into distance
		Matrix.setIdentityM(mLightModelMatrix1, 0);
		Matrix.translateM(mLightModelMatrix1, 0, 0.0f, 0.0f, -5.0f);
		Matrix.rotateM(mLightModelMatrix1, 0, angle, 1.0f, 0.0f, 0.0f);
		Matrix.translateM(mLightModelMatrix1, 0, 0.0f, 0.0f, 2.0f);

		Matrix.multiplyMV(mLightPosInWorldSpace1, 0, mLightModelMatrix1, 0, mLightPosInModelSpace1, 0);
		Matrix.multiplyMV(mLightPosInEyeSpace1, 0, mViewMatrix, 0, mLightPosInWorldSpace1, 0);
		//light source 2
		if(true)
		{
			Matrix.setIdentityM(mLightModelMatrix2, 0);
			Matrix.translateM(mLightModelMatrix2, 0, 0.0f, 0.0f, -5.0f);
			Matrix.rotateM(mLightModelMatrix2, 0, angle2, 0.0f, 2.0f, 0.0f);
			Matrix.translateM(mLightModelMatrix2, 0, 0.0f, 0.0f, 2.0f);

			Matrix.multiplyMV(mLightPosInWorldSpace2, 0, mLightModelMatrix2, 0, mLightPosInModelSpace2, 0);
			Matrix.multiplyMV(mLightPosInEyeSpace2, 0, mViewMatrix, 0, mLightPosInWorldSpace2, 0);
		}
		//Draw cubes
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 4.0f, 0.0f, -7.0f);
		Matrix.rotateM(mModelMatrix, 0, angle, 1.0f, 0.0f, 0.0f);
		drawCube();

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, -4.0f, 0.0f, -7.0f);
		Matrix.rotateM(mModelMatrix, 0, angle, 0.0f, 1.0f, 0.0f);
		drawCube();

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0.0f, 4.0f, -7.0f);
		Matrix.rotateM(mModelMatrix, 0, angle, 0.0f, 0.0f, 1.0f);
		drawCube();

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0.0f, -4.0f, -7.0f);
		drawCube();

		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.translateM(mModelMatrix, 0, 0.0f, 0.0f, -5.0f);
		Matrix.rotateM(mModelMatrix, 0, angle, 1.0f, 1.0f, 0.0f);
		drawCube();

		GLES20.glUseProgram(mPointProgramHandle);
		drawLight();
		drawLight2();
	}

	private void drawCube()
	{
		//pass in position info
		mCube1Vertices.position(0);
		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, 0, mCube1Vertices);
		GLES20.glEnableVertexAttribArray(mPositionHandle);
		//pass in color info
		mCube1Colors.position(0);
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 0, mCube1Colors);
		GLES20.glEnableVertexAttribArray(mColorHandle);
		//pass in normal info
		mCube1Normals.position(0);
		GLES20.glVertexAttribPointer(mNormalHandle, mNormalDataSize, GLES20.GL_FLOAT, false, 0, mCube1Normals);
		GLES20.glEnableVertexAttribArray(mNormalHandle);
		//this multiplies the view matrix by the model matrix and stores the result in the MVP matrix
		//which currently contains model * view
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
		//pass in the modelview matrix
		GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVPMatrix, 0);
		//this multiplies the modelview matrix by the projection matrix and stores the result in the MVP matrix
		//which now holds model * view * projection
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		GLES20.glUniform3f(mLightPos1Handle, mLightPosInEyeSpace1[0], mLightPosInEyeSpace1[1], mLightPosInEyeSpace1[2]);
		GLES20.glUniform3f(mLightPos2Handle, mLightPosInEyeSpace2[0], mLightPosInEyeSpace2[1], mLightPosInEyeSpace2[2]);

		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
	}

	private void drawLight()
	{
		final int pointMVPMatrixHandle = GLES20.glGetUniformLocation(mPointProgramHandle, "u_MVPMatrix");
		final int pointPositionHandle = GLES20.glGetAttribLocation(mPointProgramHandle, "a_Position");

		// Pass in the position.
		GLES20.glVertexAttrib3f(pointPositionHandle, mLightPosInModelSpace1[0], mLightPosInModelSpace1[1], mLightPosInModelSpace1[2]);

		// Since we are not using a buffer object, disable vertex arrays for this attribute.
		GLES20.glDisableVertexAttribArray(pointPositionHandle);  

		// Pass in the transformation matrix.
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mLightModelMatrix1, 0);
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(pointMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		// Draw the point.
		GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
	}

	private void drawLight2()
	{
		final int pointMVPMatrixHandle = GLES20.glGetUniformLocation(mPointProgramHandle, "u_MVPMatrix");
		final int pointPositionHandle = GLES20.glGetAttribLocation(mPointProgramHandle, "a_Position");

		// Pass in the position.
		GLES20.glVertexAttrib3f(pointPositionHandle, mLightPosInModelSpace2[0], mLightPosInModelSpace2[1], mLightPosInModelSpace2[2]);

		// Since we are not using a buffer object, disable vertex arrays for this attribute.
		GLES20.glDisableVertexAttribArray(pointPositionHandle);  

		// Pass in the transformation matrix.
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mLightModelMatrix2, 0);
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		GLES20.glUniformMatrix4fv(pointMVPMatrixHandle, 1, false, mMVPMatrix, 0);

		// Draw the point.
		GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
	}


	protected String getVertexShader()
	{
		final String vertexShader = 
				"uniform mat4 u_MVPMatrix;		\n"		//A constant representing the combined model/view/projection matrix
			+	"uniform mat4 u_MVMatrix;		\n"		//A constant representing the combined model/view/matrix
			
			+	"attribute vec4 a_Position;		\n"		//Per-vertex position information we will pass in
			+	"attribute vec4 a_Color;		\n"		//Per-vertex color information
			+	"attribute vec3 a_Normal;		\n"		//Per-vertex normal information

			+	"varying vec4 v_Color;			\n"		//This will be passed into the fragment shader
			+	"varying vec3 v_Position;		\n"
			+	"varying vec3 v_Normal;			\n"

			+	"void main()					\n"		//Entry point for vertex shader
			+	"{								\n"
			//Transform the vertex into eye space
			+	"	v_Position = vec3(u_MVMatrix * a_Position);				\n"
			//Transform the normal's orientation into eye space
			+	"	v_Normal = vec3(u_MVMatrix * vec4(a_Normal, 0.0));		\n"
			//Pass through color
			+	"	v_Color = a_Color;											\n"
			//It will be interpolated across the triangle
			+	"	gl_Position = u_MVPMatrix   \n"		//gl_position is a special variable used to store final position
			+	"				* a_Position;	\n"		//multiple the vertex by the matrix to get the final point in 
			+	"}								\n";		//normalized screen coordinates.             

		return vertexShader;
	}

	protected String getPixelShader()
	{
		final String fragmentShader = 
			"precision mediump float;		\n"		//Set the default precision to medium. we don't need as high of a precision
			//in the fragment shader				
			+	"uniform vec3 u_LightPos1;		\n"		//The position of the light in eye space
			+	"uniform vec3 u_LightPos2;		\n"
			+	"varying vec4 v_Color;			\n"		//This is the color from the vertex shader interpolated across the
			//triangle per fragmemt
			+	"varying vec3 v_Normal;			\n"
			+	"varying vec3 v_Position;		\n"
			+	"void main()					\n"		//Entry point for our fragment
			+	"{								\n"
			//Will be used for attenuation
			+	"	float distance1 = length(u_LightPos1 - v_Position);				\n"
			+	"	float distance2 = length(u_LightPos2 - v_Position);				\n"
			//Get a lighting direction vector from light to vertex
			+	"	vec3 lightVector1 = normalize(u_LightPos1 - v_Position);			\n"
			+	"	vec3 lightVector2 = normalize(u_LightPos2 - v_Position);			\n"
			//Calculate  the dot product of the light vector and the vertex normal. If the normal and light vector are
			//pointing in the same direction then it will get max illumination
			+	"	float diffuse1 = max(dot(v_Normal, lightVector1), 0.1);		\n"
			+	"	float diffuse2 = max(dot(v_Normal, lightVector2), 0.1);		\n"
			//Attenuate the light based on distance
			+	"	diffuse1 = diffuse1 * (1.0 / (1.0 + (0.25 * distance1 * distance1)));	\n"
			+	"	diffuse2 = diffuse2 * (1.0 / (1.0 + (0.25 * distance2 * distance2)));	\n"
			//Multiply the color by the illumination level
			+	"	gl_FragColor = v_Color * (diffuse2+diffuse1);			\n"		//Pass color directly through the pipeline
			+	"}								\n";

		return fragmentShader;
	}

	public int setProgram(int vertex, int fragment, String[] params)
	{
		int programHandle = GLES20.glCreateProgram();
		if (programHandle != 0)
		{
			//Bind the vertex shader to the program
			GLES20.glAttachShader(programHandle, vertex);

			//Bind the fragment shader to the program
			GLES20.glAttachShader(programHandle, fragment);

			//Bind attributes
			for (int i = 0; i < params.length; i++)
				GLES20.glBindAttribLocation(programHandle, i, params[i]);


			//link two shaders into program
			GLES20.glLinkProgram(programHandle);

			//get link status
			final int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

			//if failure, delete program
			if (linkStatus[0] == 0)
			{
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}

		if (programHandle == 0)
		{
			throw new RuntimeException("Error creating program");
		}

		GLES20.glGetProgramInfoLog(programHandle);

		return programHandle;
	}

	public int loadShader(int shaderInt, String shader)
	{
		int shaderHandle = GLES20.glCreateShader(shaderInt);

		if (shaderHandle != 0)
		{
			//Pass in shader source
			GLES20.glShaderSource(shaderHandle, shader);

			//Compile the shader
			GLES20.glCompileShader(shaderHandle);

			//Get compile status
			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

			//if compile failed, delete shader
			if (compileStatus[0] == 0)
			{
				GLES20.glDeleteShader(shaderHandle);
				shaderHandle = 0;
			}
		}

		if (shaderHandle == 0)
		{
			throw new RuntimeException("Error creating vertex shader: " + GLES20.glGetShaderInfoLog(shaderHandle));
		}

		return shaderHandle;
	}



}
