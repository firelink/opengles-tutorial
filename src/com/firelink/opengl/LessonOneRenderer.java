package com.firelink.opengl;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.*;
import javax.microedition.khronos.egl.*;
import android.content.*;
import java.nio.*;
import android.opengl.GLES20;
import android.graphics.*;
import android.opengl.Matrix;
import android.os.*;

public class LessonOneRenderer implements GLSurfaceView.Renderer
{
	private final FloatBuffer mTriangle1Vertices;
	private final FloatBuffer mTriangle2Vertices;
	private final FloatBuffer mTriangle3Vertices;
	private final FloatBuffer mTriangle4Vertices;
	
	private float[] mViewMatrix = new float[16];
	
	//Used to pass in transformation matrix
	private int mMVPMatrixHandle;
	//pass in model position info
	private int mPositionHandle;
	//used to pass in color info
	private int mColorHandle;
	
	//Store the projection. this is used to project the scene onto a 2D viewport
	private float[] mProjectionMatrix = new float[16];
	
	//store the model matrix. this matrix is used to move models from object space (where each model can be thought
	//of being located at the center of the universe) to world space
	private float[] mModelMatrix = new float[16];
	
	private final int mBytesPerFloat = 4;
	
	//allocate storage for the final combined matrix. Passed onto the shader program
	private float[] mMVPMatrix = new float[16];
	//How many elements per vertex
	private final int mStrideBytes = 7 * mBytesPerFloat;
	//offset of thenposition data
	private final int mPositionOffset = 0;
	//size of the position data in elements
	private final int mPositionDataSize = 3;
	//offset of the color data
	private final int mColorOffset = 3;
	//size of color data in elements
	private final int mColorDataSize = 4;
	

	public LessonOneRenderer(Context c)
	{
		//This triangle is red, green and blue
		final float[] triangle1VerticesData = {
			//X, Y, Z
			//R, G, B, A
			0.5f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f,
			
			-0.5f,		0.5f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f,
			
			-0.5f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f
		};
		
		final float[] triangle2VerticesData = {
			//X, Y, Z
			//R, G, B, A
			-0.5f,	0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f,

			0.5f,		0.5f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.3f,

			0.5f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f
		};
		
		final float[] triangle3VerticesData = {
			//X, Y, Z
			//R, G, B, A
			0.0f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f,

			-0.5f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.3f,

			0.0f,		-0.75f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f
		};
		
		final float[] triangle4VerticesData = {
			//X, Y, Z
			//R, G, B, A
			0.0f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f,

			0.5f,		0.0f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.3f,

			0.0f,		-0.75f,		0.0f,
			1.0f,		0.0f,		1.0f,		0.0f
		};
		
		//initialize the buffers
		mTriangle1Vertices = ByteBuffer.allocateDirect(triangle1VerticesData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTriangle2Vertices = ByteBuffer.allocateDirect(triangle2VerticesData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTriangle3Vertices = ByteBuffer.allocateDirect(triangle3VerticesData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTriangle4Vertices = ByteBuffer.allocateDirect(triangle4VerticesData.length * mBytesPerFloat).order(ByteOrder.nativeOrder()).asFloatBuffer();
		
		mTriangle1Vertices.put(triangle1VerticesData).position(0);
		mTriangle2Vertices.put(triangle2VerticesData).position(0);
		mTriangle3Vertices.put(triangle3VerticesData).position(0);
		mTriangle4Vertices.put(triangle4VerticesData).position(0);
	}
	
	public void onSurfaceCreated(GL10 p1, EGLConfig p2)
	{
		//Set background color to gray
		GLES20.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
		
		
		//position the eye behind the origin
		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = 1.5f;
		
		//looking towards the distance
		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = -5.0f;
		
		//Set up the vector: This is where our head would be pointing were we holding the camera
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;
		
		//Set the view matrix. This can be said to be the camera's position
		//NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model
		//and a view matrix. In OGL2, we can keep track of these matrices separately if we choose
		android.opengl.Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
		
		
		final String vertexShader = 
			"uniform mat4 u_MVPMatrix;		\n"		//A constant representing the combined model/view/projection matrix

			+	"attribute vec4 a_Position;		\n"		//Per-vertex position information we will pass in
			+	"attribute vec4 a_Color;		\n"		//Per-vertex color information

			+	"varying vec4 v_Color;			\n"		//This will be passed into the fragment shader

			+	"void main()					\n"		//Entry point for vertex shader
			+	"{								\n"
			+	"	v_Color = a_Color;			\n"		//Pass color throug to fragment shader
			//It will be interpolated across the triangle
			+	"	gl_Position = u_MVPMatrix   \n"		//gl_position is a special variable used to store final position
			+	"				* a_Position;	\n"		//multiple the vertex by the matrix to get the final point in 
			+	"}								\n";		//normalized screen coordinates.
		
		final String fragmentShader = 
				"precision mediump float;		\n"		//Set the default precision to medium. we don't need as high of a precision
														//in the fragment shader				
			+	"varying vec4 v_Color;			\n"		//This is the color from the vertex shader interpolated across the
														//triangle per fragmemt
			+	"void main()					\n"		//Entry point for our fragment
			+	"{								\n"
			+	"	gl_FragColor = v_Color;		\n"		//Pass color directly through the pipeline
			+	"}								\n";
			
			
		int vertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
		
		if(vertexShaderHandle != 0)
		{
			//Pass in shader source
			GLES20.glShaderSource(vertexShaderHandle, vertexShader);
			
			//Compile the shader
			GLES20.glCompileShader(vertexShaderHandle);
			
			//Get compile status
			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(vertexShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
			
			//if compile failed, delete shader
			if(compileStatus[0] == 0)
			{
				GLES20.glDeleteShader(vertexShaderHandle);
				vertexShaderHandle = 0;
			}
		}
		
		if(vertexShaderHandle == 0)
		{
			throw new RuntimeException("Error creating vertex shader");
		}
		
		
		int fragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);

		if(fragmentShaderHandle != 0)
		{
			//Pass in shader source
			GLES20.glShaderSource(fragmentShaderHandle, fragmentShader);

			//Compile the shader
			GLES20.glCompileShader(fragmentShaderHandle);

			//Get compile status
			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(fragmentShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

			//if compile failed, delete shader
			if(compileStatus[0] == 0)
			{
				GLES20.glDeleteShader(fragmentShaderHandle);
				fragmentShaderHandle = 0;
			}
		}

		if(fragmentShaderHandle == 0)
		{
			throw new RuntimeException("Error creating fragment shader");
		}
		
		
		//Create a program object and store the handle to it
		int programHandle = GLES20.glCreateProgram();
		
		if(programHandle != 0)
		{
			//Bind the vertex shader to the program
			GLES20.glAttachShader(programHandle, vertexShaderHandle);
			
			//Bind the fragment shader to the program
			GLES20.glAttachShader(programHandle, fragmentShaderHandle);
			
			//Bind attributes
			GLES20.glBindAttribLocation(programHandle, 0, "a_Position");
			GLES20.glBindAttribLocation(programHandle, 1, "a_Color");
			
			//link two shaders into program
			GLES20.glLinkProgram(programHandle);
			
			//get link status
			final int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);
			
			//if failure, delete program
			if(linkStatus[0] == 0)
			{
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}
		
		if(programHandle == 0)
		{
			throw new RuntimeException("Error creating program");
		}
		
		//Set program handles. These will be later used to pass in values to the program
		mMVPMatrixHandle = GLES20.glGetUniformLocation(programHandle, "u_MVPMatrix");
		mPositionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
		mColorHandle = GLES20.glGetAttribLocation(programHandle, "a_Color");
		
		//tell OGL to use this program when rendering
		GLES20.glUseProgram(programHandle);
		
		
		
	}

	public void onSurfaceChanged(GL10 p1, int width, int height)
	{
		//Set OGL viewport
		GLES20.glViewport(0, 0, width, height);
		
		//Create a new perspective projection matrix. The height will stay the same
		//while the width will vary as per aspect ratio
		final float ratio = (float)width/height;
		final float left = -ratio;
		final float right = ratio;
		final float bottom = -1.0f;
		final float top = 1.0f;
		final float near = 1.0f;
		final float far = 10.0f;
		
		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}

	public void onDrawFrame(GL10 p1)
	{
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		
		//complete rotaton every 10 seconds
		long time = SystemClock.uptimeMillis() % 10000L;
		float angle = (360.0f/10000.0f) * ((int)time);
		
		//draw triangle
		Matrix.setIdentityM(mModelMatrix, 0);
//		Matrix.rotateM(mModelMatrix, 0, angle, 0.0f, 0.0f, 1.0f);
		drawTriangle(mTriangle1Vertices);
		drawTriangle(mTriangle2Vertices);
		drawTriangle(mTriangle3Vertices);
		drawTriangle(mTriangle4Vertices);
	}
	
	private void drawTriangle(final FloatBuffer aTriangleBuffer)
	{
		//pass in position info
		aTriangleBuffer.position(mPositionOffset);
		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, mStrideBytes, aTriangleBuffer);
		
		GLES20.glEnableVertexAttribArray(mPositionHandle);
		
		//pass in color info
		aTriangleBuffer.position(mColorOffset);
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, mStrideBytes, aTriangleBuffer);
		
		GLES20.glEnableVertexAttribArray(mColorHandle);
		
		//this multiplies the view matrix by the model matrix and stores the result in the MVP matrix
		//which currently contains model * view
		Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
		
		//this multiplies the modelview matrix by the projection matrix and stores the result in the MVP matrix
		//which now holds model * view * projection
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		
		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3);
	}
	
	

	
}
